package com.pxlpe.chatapppe;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import net.danlew.android.joda.JodaTimeAndroid;

public class MyApplication extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        Logger.addLogAdapter(new AndroidLogAdapter());

        mContext = getApplicationContext();
    }

    public static Context getContext(){
        return mContext;
    }
}
