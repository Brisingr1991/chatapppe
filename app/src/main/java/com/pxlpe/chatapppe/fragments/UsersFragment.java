package com.pxlpe.chatapppe.fragments;


import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.MyApplication;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;
import com.pxlpe.chatapppe.models.User;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.pxlpe.chatapppe.Constants.CHATS;
import static com.pxlpe.chatapppe.Constants.CHAT_ID;
import static com.pxlpe.chatapppe.Constants.FRIEND_ID;
import static com.pxlpe.chatapppe.Constants.USERS;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment {
    public static final String TAG = "UsersFragment";
    private MainActivity parentActivity;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mUsersDatabase;

    @BindView(R.id.usersList)
    RecyclerView usersList;
    @BindView(R.id.emptyView)
    TextView emptyView;

    public static UsersFragment newInstance() {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public UsersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<User, UsersViewHolder> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<User, UsersViewHolder>(
                        User.class, R.layout.item_list_users, UsersViewHolder.class, mUsersDatabase) {
                    @Override
                    protected void populateViewHolder(UsersViewHolder viewHolder, User model, final int position) {
                        if (!mCurrentUser.getUid().equals(getRef(position).getKey())) {
                            viewHolder.setFirstName(model.getFirst_name());
                            viewHolder.setLastName(model.getLast_name());
                            if (model.getEncoded_picture() != null) {
                                viewHolder.setUserPicture(model.getEncoded_picture());
                            }
                            final String friend_id = getRef(position).getKey();

                            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    CharSequence options[] = new CharSequence[]{"Open Profile", "Send Message"};

                                    AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
                                    builder.setTitle("Select options");
                                    builder.setItems(options, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            switch (i) {
                                                case 0:
                                                    Helper_Fragments.replaceFragment(parentActivity, ProfileFragment.newInstance(getRef(position).getKey()), true, ProfileFragment.TAG);
                                                    break;
                                                case 1:
                                                    mUsersDatabase.child(mCurrentUser.getUid()).child(CHATS).child(friend_id).addValueEventListener(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.hasChild(CHAT_ID)) {
                                                                Object obj = dataSnapshot.child(CHAT_ID).getValue();
                                                                if (obj != null) {
                                                                    Helper_Fragments.replaceFragment(parentActivity, ConversationFragment.newInstance(obj.toString(), friend_id), true, ConversationFragment.TAG);
                                                                }
                                                            } else {
                                                                Map<String, Object> newChats = new HashMap<>();

                                                                final String pushKey = FirebaseDatabase.getInstance().getReference("chats").push().getKey();
                                                                Map<String, String> updateChats = new HashMap<>();
                                                                updateChats.put(CHAT_ID, pushKey);
                                                                updateChats.put(FRIEND_ID, friend_id);

                                                                Map<String, String> updateFriendChats = new HashMap<>();
                                                                updateFriendChats.put(CHAT_ID, pushKey);
                                                                updateFriendChats.put(FRIEND_ID, mCurrentUser.getUid());

                                                                newChats.put(CHATS + "/" + pushKey, updateChats);
                                                                newChats.put(USERS + "/" + mCurrentUser.getUid() + "/" + CHATS + "/" + friend_id, updateChats);
                                                                newChats.put(USERS + "/" + friend_id + "/" + CHATS + "/" + mCurrentUser.getUid(), updateFriendChats);

                                                                FirebaseDatabase.getInstance().getReference().updateChildren(newChats, new DatabaseReference.CompletionListener() {
                                                                    @Override
                                                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                                                        //TODO: this calls the fragment 2 times, check what happens with other user you want to talk to, if the fragment pops up without your permission
//                                                                        Helper_Fragments.replaceFragment(parentActivity, ConversationFragment.newInstance(pushKey, friend_id), true, ConversationFragment.TAG);
                                                                    }
                                                                });
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                        }
                                                    });

                                                    break;
                                            }
                                        }
                                    });
                                    builder.show();
                                }
                            });
                        } else {
                            viewHolder.mView.findViewById(R.id.extraLayout).setVisibility(View.GONE);
                        }

                        updateLayout();
                    }
                };

        usersList.setAdapter(firebaseRecyclerAdapter);

        updateLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setBottomNavBar(true);
        parentActivity.setTitleToolbar("Users");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_users, container, false);
        parentActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, v);

        parentActivity.setBottomNavBar(true);
        parentActivity.setTitleToolbar("Users");

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        usersList.setHasFixedSize(true);
        usersList.setLayoutManager(new LinearLayoutManager(parentActivity));

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child(USERS);

        return v;
    }

    private void updateLayout() {
        if (usersList.getAdapter().getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setFirstName(String firstName) {
            TextView firstNameUser = mView.findViewById(R.id.firstNameTv);
            firstNameUser.setText(firstName);
        }

        void setLastName(String lastName) {
            TextView lastNameUser = mView.findViewById(R.id.lastNameTv);
            lastNameUser.setText(lastName);
        }

        void setUserPicture(String pictureEncoded) {
            CircleImageView circleImageView = mView.findViewById(R.id.userProfileIcon);
            byte[] decodedByte = Base64.decode(pictureEncoded, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
            Glide.with(MyApplication.getContext()).load(bitmap).into(circleImageView);
        }
    }
}