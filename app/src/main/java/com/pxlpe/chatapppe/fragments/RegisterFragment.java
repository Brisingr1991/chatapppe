package com.pxlpe.chatapppe.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.logger.Logger;
import com.pxlpe.chatapppe.Helpers.Helper_Security;
import com.pxlpe.chatapppe.activities.LoginActivity;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;

import java.math.BigInteger;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.pxlpe.chatapppe.Constants.FIRST_NAME;
import static com.pxlpe.chatapppe.Constants.LAST_NAME;
import static com.pxlpe.chatapppe.Constants.PUBLIC_EXPONENT;
import static com.pxlpe.chatapppe.Constants.PUBLIC_MODULUS;
import static com.pxlpe.chatapppe.Constants.USERS;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    public static final String TAG = "RegisterFragment";
    private LoginActivity parentActivity;
    private FirebaseAuth mAuth;
    private boolean mIsAllFilled = true;

    //<editor-fold desc="BindView">
    @BindView(R.id.firstNameInputEt)
    TextInputEditText firstNameInputEt;
    @BindView(R.id.firstNameInputLayout)
    TextInputLayout firstNameInputLayout;
    @BindView(R.id.lastNameInputEt)
    TextInputEditText lastNameInputEt;
    @BindView(R.id.lastNameInputLayout)
    TextInputLayout lastNameInputLayout;
    @BindView(R.id.emailInputEt)
    TextInputEditText emailInputEt;
    @BindView(R.id.emailInputLayout)
    TextInputLayout emailInputLayout;
    @BindView(R.id.passwordInputEt)
    TextInputEditText passwordInputEt;
    @BindView(R.id.passwordInputLayout)
    TextInputLayout passwordInputLayout;
    @BindView(R.id.registerBtn)
    Button registerBtn;
    @BindView(R.id.goBackTv)
    TextView goBackTv;
    //</editor-fold>
    //<editor-fold desc="OnClicks">
    @OnClick(R.id.registerBtn)
    public void onLoginBtnClicked() {
        registerBtn.setEnabled(false);

        checkIfErrorNeeded(firstNameInputLayout, firstNameInputEt);
        checkIfErrorNeeded(lastNameInputLayout, lastNameInputEt);
        checkIfErrorNeeded(emailInputLayout, emailInputEt);
        checkIfErrorNeeded(passwordInputLayout, passwordInputEt);

        if (!mIsAllFilled) {
            mIsAllFilled = true;
            registerBtn.setEnabled(true);
            return;
        }

        final MaterialDialog dialogProgress = new MaterialDialog.Builder(parentActivity)
                .autoDismiss(false).cancelable(false)
                .title("Inloggen...")
                .content("Even geduld.")
                .progress(true, 0)
                .build();
        dialogProgress.show();

        mAuth.createUserWithEmailAndPassword(emailInputEt.getText().toString(), passwordInputEt.getText().toString())
                .addOnCompleteListener(parentActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference myRef = database.getReference(USERS).child(user.getUid());

                                PublicKey publicKey = Helper_Security.createKeyPairsOrGetPublicKey(user.getUid());

                                if (publicKey != null) {
                                    RSAPublicKey rsaKey = (RSAPublicKey) publicKey;
                                    BigInteger publicExponent = rsaKey.getPublicExponent();
                                    BigInteger publicModulus = rsaKey.getModulus();

                                    HashMap<String, String> userMap = new HashMap<>();
                                    userMap.put(FIRST_NAME, firstNameInputEt.getText().toString());
                                    userMap.put(LAST_NAME, lastNameInputEt.getText().toString());
                                    userMap.put(PUBLIC_EXPONENT, publicExponent.toString());
                                    userMap.put(PUBLIC_MODULUS, publicModulus.toString());

                                    myRef.setValue(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Intent intent = new Intent(parentActivity, MainActivity.class);
                                            startActivity(intent);
                                            parentActivity.finish();
                                        }
                                    });
                                } else {
                                    Toast.makeText(parentActivity, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }

                                dialogProgress.dismiss();
                                registerBtn.setEnabled(true);
                            }
                        } else {
                            Logger.d(task.getException());

                            if (task.getException() != null) {
                                Toast.makeText(parentActivity, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }

                            dialogProgress.dismiss();
                            registerBtn.setEnabled(true);
                        }
                    }
                });
    }

    @OnClick(R.id.goBackTv)
    public void onGoBackTvClicked() {
        Helper_Fragments.popBackstack(parentActivity);
    }
    //</editor-fold>

    private void checkIfErrorNeeded(TextInputLayout inputLayout, TextInputEditText inputEt) {
        if (TextUtils.isEmpty(inputEt.getText())) {
            inputLayout.setError("This is required");
            mIsAllFilled = false;
        } else {
            inputLayout.setErrorEnabled(false);
        }
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        parentActivity = (LoginActivity) getActivity();
        ButterKnife.bind(this, v);
        mAuth = FirebaseAuth.getInstance();

        return v;
    }
}
