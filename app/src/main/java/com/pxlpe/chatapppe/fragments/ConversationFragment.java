package com.pxlpe.chatapppe.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseIndexRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.logger.Logger;
import com.pxlpe.chatapppe.Helpers.Helper_Security;
import com.pxlpe.chatapppe.MyApplication;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;
import com.pxlpe.chatapppe.models.Message;
import com.pxlpe.chatapppe.models.User;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.bullyboo.encoder.Encoder;
import ru.bullyboo.encoder.methods.DES;

import static com.pxlpe.chatapppe.Constants.CHAT_ID;
import static com.pxlpe.chatapppe.Constants.FRIEND_ID;
import static com.pxlpe.chatapppe.Constants.MESSAGES;
import static com.pxlpe.chatapppe.Constants.USERS;

public class ConversationFragment extends Fragment {
    public static final String TAG = "ConversationFragment";
    private MainActivity parentActivity;
    private String chatId;
    private DatabaseReference mMessagesRef;
    private FirebaseUser mCurrentUser;
    private String friendPublicModulus, friendPublicExponent;
    private PrivateKey mPrivateKey;
    private PublicKey mPublicKey;
    private PublicKey mFriendPublicKey;
    private String mFriendEncodedPicture, mUserEncodedPicture;

    @BindView(R.id.conversationList)
    RecyclerView conversationList;
    @BindView(R.id.messageEt)
    EditText messageEt;
    @BindView(R.id.sendBtn)
    ImageButton sendBtn;
    @BindView(R.id.inputLayout)
    LinearLayout inputLayout;
    @BindView(R.id.emptyView)
    TextView emptyView;

    @OnClick(R.id.sendBtn)
    public void onViewClicked() {
        sendBtn.setEnabled(false);

        if (!TextUtils.isEmpty(messageEt.getText().toString())) {
            String mPushKey = mMessagesRef.push().getKey();
            //TODO: make the symKey dynamic which contains only letters, numbers will make it crash or don't work
            String mSymKey = "ThisIsTheSymKey";
            try {
                //<editor-fold desc="encrypt with sym key">
                String encryptedMessage = Encoder.BuilderDES()
                        .method(DES.Method.DES_CBC_ISO10126Padding)
                        .key(mSymKey)
                        .message(messageEt.getText().toString())
                        .encrypt();
                //</editor-fold>

                //TODO: text stays when sending
                //<editor-fold desc="encrypt the sym key with user public key">
                String encryptedSymKeySender = Helper_Security.encryptWithPublicKey(mPublicKey, mSymKey);
                //</editor-fold>

                //<editor-fold desc="encrypt the sym key with friend public key">
                String encryptedSymKeyFriend = Helper_Security.encryptWithPublicKey(mFriendPublicKey, mSymKey);
                //</editor-fold>

                //<editor-fold desc="get hash from input text">
                String hashFromMessage = Encoder.Hashes().md5(messageEt.getText().toString());
                Logger.d("get hash from message sending: " + hashFromMessage);
                //</editor-fold>

                //<editor-fold desc="sign hash with private key">
                String signedHashFromMessage = Helper_Security.signWithPrivateKey(mPrivateKey, hashFromMessage);
                //</editor-fold>

                Message message = new Message();
                message.setSender(mCurrentUser.getUid());
                message.setTimestamp(System.currentTimeMillis() / 1000L);
                message.setEncrypted_message(encryptedMessage);
                message.setEncrypted_sym_key_friend(encryptedSymKeyFriend);
                message.setSigned_hash_from_message(signedHashFromMessage);
                message.setEncrypted_sym_key_sender(encryptedSymKeySender);

                HashMap<String, Object> messageItemMap = new HashMap<>();
                messageItemMap.put("/" + mPushKey, message);

                mMessagesRef.updateChildren(messageItemMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        messageEt.setText("");
                        sendBtn.setEnabled(true);

                        // Only scroll to the bottom when the user send something themselves
                        conversationList.getAdapter().registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onItemRangeInserted(int positionStart, int itemCount) {
                                super.onItemRangeInserted(positionStart, itemCount);
                                conversationList.smoothScrollToPosition(positionStart);
                            }
                        });
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static ConversationFragment newInstance(String chatId, String friendId) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putString(CHAT_ID, chatId);
        args.putString(FRIEND_ID, friendId);
        fragment.setArguments(args);
        return fragment;
    }

    public ConversationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setBottomNavBar(false);
    }

    @Override
    public void onStart() {
        super.onStart();

//        conversationList.getRecycledViewPool().clear();

        FirebaseIndexRecyclerAdapter<Message, MessagesViewHolder> firebaseIndexRecyclerAdapter =
                new FirebaseIndexRecyclerAdapter<Message, MessagesViewHolder>
                        (Message.class, R.layout.item_list_messages, MessagesViewHolder.class, mMessagesRef, mMessagesRef) {
                    @Override
                    protected void populateViewHolder(final MessagesViewHolder viewHolder, final Message model, int position) {
                        final String sender = model.getSender();
                        long timestamp = model.getTimestamp();
                        String encrypted_message = model.getEncrypted_message();
                        String encrypted_sym_key_friend = model.getEncrypted_sym_key_friend();
                        String encrypted_sym_key_sender = model.getEncrypted_sym_key_sender();
                        String signed_hash_from_message = model.getSigned_hash_from_message();

                        //if i am the sender
                        if (sender.equals(mCurrentUser.getUid())) {
                            //<editor-fold desc="decrypt sym key with private key">
                            String decryptedSymKeySender = Helper_Security.decryptWithPrivateKey(mPrivateKey, encrypted_sym_key_sender);
                            //</editor-fold>

                            //<editor-fold desc="decrypt message with sym key">
                            String decryptedMessageWithSymKey = "";
                            if (!TextUtils.isEmpty(decryptedSymKeySender)) {
                                decryptedMessageWithSymKey = Encoder.BuilderDES()
                                        .key(decryptedSymKeySender)
                                        .message(encrypted_message)
                                        .method(DES.Method.DES_CBC_ISO10126Padding)
                                        .decrypt();
                            }
                            //</editor-fold>

                            //<editor-fold desc="get hash from decrypted message">
                            String hashFromDecryptedMessage = "";
                            if (!TextUtils.isEmpty(decryptedMessageWithSymKey)) {
                                hashFromDecryptedMessage = Encoder.Hashes().md5(decryptedMessageWithSymKey);
                                Logger.d("hash from decrypted message: " + hashFromDecryptedMessage);
                            }
                            //</editor-fold>

                            //<editor-fold desc="verify hash with public key">
                            boolean isVerified = false;
                            if (hashFromDecryptedMessage != null || !TextUtils.isEmpty(hashFromDecryptedMessage)) {
                                isVerified = Helper_Security.verifyWithPublicKey(mPublicKey, hashFromDecryptedMessage, signed_hash_from_message);
                            }
                            //</editor-fold>

                            if (isVerified) {
                                viewHolder.setUserBody(decryptedMessageWithSymKey);
                            } else {
                                viewHolder.setUserBody("This message has been deleted");
                            }
                            if (mUserEncodedPicture != null) {
                                viewHolder.setUserPicture(mUserEncodedPicture);
                            }
                            viewHolder.setUserTimestamp(timestamp + "");
                            viewHolder.setUserLayoutVisible(true);
                            viewHolder.setFriendLayoutVisible(false);

                        } else {
                            //<editor-fold desc="decrypt sym key with private key">
                            String decryptedSymKeyFriend = Helper_Security.decryptWithPrivateKey(mPrivateKey, encrypted_sym_key_friend);
                            //</editor-fold>

                            //<editor-fold desc="decrypt message with sym key">
                            String decryptedMessageWithSymKey = "";
                            if (!TextUtils.isEmpty(decryptedSymKeyFriend)) {
                                decryptedMessageWithSymKey = Encoder.BuilderDES()
                                        .key(decryptedSymKeyFriend)
                                        .message(encrypted_message)
                                        .method(DES.Method.DES_CBC_ISO10126Padding)
                                        .decrypt();
                            }
                            //</editor-fold>

                            //<editor-fold desc="get hash from decrypted message">
                            String hashFromDecryptedMessage = "";
                            if (decryptedMessageWithSymKey != null || !TextUtils.isEmpty(decryptedMessageWithSymKey)) {
                                hashFromDecryptedMessage = Encoder.Hashes().md5(decryptedMessageWithSymKey);
                                Logger.d("hash from decrypted message: " + hashFromDecryptedMessage);
                            }
                            //</editor-fold>

                            //<editor-fold desc="verify hash with public key">
                            boolean isVerified = false;
                            if (hashFromDecryptedMessage != null || !TextUtils.isEmpty(hashFromDecryptedMessage)) {
                                isVerified = Helper_Security.verifyWithPublicKey(mFriendPublicKey, hashFromDecryptedMessage, signed_hash_from_message);
                            }
                            //</editor-fold>

                            if (isVerified) {
                                viewHolder.setFriendBody(decryptedMessageWithSymKey);
                            } else {
                                viewHolder.setFriendBody("This message has been deleted");
                            }
                            if (mFriendEncodedPicture != null) {
                                viewHolder.setFriendPicture(mFriendEncodedPicture);
                            }
                            viewHolder.setFriendTimestamp(timestamp + "");
                            viewHolder.setUserLayoutVisible(false);
                            viewHolder.setFriendLayoutVisible(true);
                        }
                        updateLayout();
                    }
                };

        conversationList.setAdapter(firebaseIndexRecyclerAdapter);

        updateLayout();
    }

    private void updateLayout() {
        if (conversationList.getAdapter().getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_conversation, container, false);
        parentActivity = (MainActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.setBottomNavBar(false);
        }
        ButterKnife.bind(this, v);
        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();

        if (getArguments() != null) {
            String friendId = getArguments().getString(FRIEND_ID);
            chatId = getArguments().getString(CHAT_ID);

            if (friendId != null) {
                mRootRef.child(USERS).child(friendId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);

                        if (user != null) {
                            friendPublicExponent = user.getPublic_exponent();
                            friendPublicModulus = user.getPublic_modulus();
                            parentActivity.setTitleToolbar(user.getFirst_name() + " " + user.getLast_name());
                            mFriendPublicKey = Helper_Security.convertStringToPublicKey(friendPublicExponent, friendPublicModulus);
                            if (user.getEncoded_picture() != null) {
                                mFriendEncodedPicture = user.getEncoded_picture();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mMessagesRef = FirebaseDatabase.getInstance().getReference().child(MESSAGES + "/" + chatId);


        mRootRef.child(USERS).child(mCurrentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    if (user.getEncoded_picture() != null) {
                        mUserEncodedPicture = user.getEncoded_picture();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);

            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(mCurrentUser.getUid(), null);
            mPrivateKey = privateKeyEntry.getPrivateKey();
            mPublicKey = privateKeyEntry.getCertificate().getPublicKey();

            byte[] bytes = mPublicKey.getEncoded();
            StringBuilder builder = new StringBuilder();
            for (byte bit :
                    bytes) {
                builder.append(bit);
            }
            Logger.d(builder.toString());
        } catch (IOException | NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException | CertificateException e) {
            e.printStackTrace();
        }

        conversationList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(parentActivity);
        layoutManager.setStackFromEnd(true);
        conversationList.setLayoutManager(layoutManager);

        return v;
    }


    public static class MessagesViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public MessagesViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setUserBody(String body) {
            TextView userBodyTv = mView.findViewById(R.id.userBodyTv);
            userBodyTv.setText(body);
        }

        void setUserTimestamp(String timestamp) {
            TextView userTimestampTv = mView.findViewById(R.id.userTimestampTv);

            DateTime dateTime = new DateTime(Long.parseLong(timestamp) * 1000L);
            userTimestampTv.setText(dateTime.toLocalTime().toString(DateTimeFormat.shortTime()));
        }

        void setFriendBody(String body) {
            TextView friendBodyTv = mView.findViewById(R.id.friendBodyTv);
            friendBodyTv.setText(body);
        }

        void setFriendTimestamp(String timestamp) {
            TextView friendTimestampTv = mView.findViewById(R.id.friendTimestampTv);

            DateTime dateTime = new DateTime(Long.parseLong(timestamp) * 1000L);
            friendTimestampTv.setText(dateTime.toLocalTime().toString(DateTimeFormat.shortTime()));
        }

        void setFriendLayoutVisible(boolean isVisible) {
            ConstraintLayout layout = mView.findViewById(R.id.friendMessage);
            if (isVisible) {
                layout.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.GONE);
            }
        }

        void setUserLayoutVisible(boolean isVisible) {
            ConstraintLayout layout = mView.findViewById(R.id.userMessage);
            if (isVisible) {
                layout.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.GONE);
            }
        }

        void setUserPicture(String pictureEncoded) {
            CircleImageView circleImageView = mView.findViewById(R.id.userImg);
            byte[] decodedByte = Base64.decode(pictureEncoded, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
            Glide.with(MyApplication.getContext()).load(bitmap).into(circleImageView);
        }

        void setFriendPicture(String pictureEncoded) {
            CircleImageView circleImageView = mView.findViewById(R.id.friendImg);
            byte[] decodedByte = Base64.decode(pictureEncoded, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
            Glide.with(MyApplication.getContext()).load(bitmap).into(circleImageView);
        }
    }
}
