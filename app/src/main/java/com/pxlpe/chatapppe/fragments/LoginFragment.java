package com.pxlpe.chatapppe.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orhanobut.logger.Logger;
import com.pxlpe.chatapppe.Helpers.Helper_Security;
import com.pxlpe.chatapppe.activities.LoginActivity;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;

import java.math.BigInteger;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.pxlpe.chatapppe.Constants.PUBLIC_EXPONENT;
import static com.pxlpe.chatapppe.Constants.PUBLIC_MODULUS;
import static com.pxlpe.chatapppe.Constants.USERS;

public class LoginFragment extends Fragment {
    public static final String TAG = "LoginFragment";
    private LoginActivity parentActivity;
    private FirebaseAuth mAuth;
    private boolean mIsAllFilled = true;

    //<editor-fold desc="BindView">
    @BindView(R.id.emailInputEt)
    TextInputEditText emailInputEt;
    @BindView(R.id.emailInputLayout)
    TextInputLayout emailInputLayout;
    @BindView(R.id.passwordInputEt)
    TextInputEditText passwordInputEt;
    @BindView(R.id.passwordInputLayout)
    TextInputLayout passwordInputLayout;
    @BindView(R.id.loginBtn)
    Button loginBtn;
    @BindView(R.id.forgotPasswordTv)
    TextView forgotPasswordTv;
    @BindView(R.id.noAccountTv)
    TextView noAccountTv;

    //</editor-fold>
    //<editor-fold desc="OnClicks">
    @OnClick(R.id.loginBtn)
    public void onLoginBtnClicked() {
        loginBtn.setEnabled(false);

        checkIfErrorNeeded(emailInputLayout, emailInputEt);
        checkIfErrorNeeded(passwordInputLayout, passwordInputEt);

        if (!mIsAllFilled) {
            loginBtn.setEnabled(true);
            return;
        }

        final MaterialDialog dialogProgress = new MaterialDialog.Builder(parentActivity)
                .autoDismiss(false).cancelable(false)
                .title("Inloggen...")
                .content("Even geduld.")
                .progress(true, 0)
                .build();
        dialogProgress.show();

        mAuth.signInWithEmailAndPassword(emailInputEt.getText().toString(), passwordInputEt.getText().toString())
                .addOnCompleteListener(parentActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String Uid = task.getResult().getUser().getUid();

                            PublicKey publicKey = Helper_Security.createKeyPairsOrGetPublicKey(Uid);

                            if (publicKey != null) {
                                RSAPublicKey rsaKey = (RSAPublicKey) publicKey;
                                BigInteger publicExponent = rsaKey.getPublicExponent();
                                BigInteger publicModulus = rsaKey.getModulus();

                                Map<String, Object> keyMap = new HashMap<>();
                                keyMap.put(PUBLIC_EXPONENT, publicExponent.toString());
                                keyMap.put(PUBLIC_MODULUS, publicModulus.toString());

                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference(USERS).child(Uid);

                                ref.updateChildren(keyMap, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                        Intent intent = new Intent(parentActivity, MainActivity.class);
                                        startActivity(intent);
                                        parentActivity.finish();
                                    }
                                });

                            } else {
                                Toast.makeText(parentActivity, "Something went wrong!", Toast.LENGTH_SHORT).show();
                            }

                            loginBtn.setEnabled(true);
                            dialogProgress.dismiss();
                        } else {
                            Logger.d(task.getException());
                            if (task.getException() != null) {
                                Toast.makeText(parentActivity, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }

                            loginBtn.setEnabled(true);
                            dialogProgress.dismiss();
                        }
                    }
                });
    }

    @OnClick(R.id.forgotPasswordTv)
    public void onForgotPasswordTvClicked() {
        Toast.makeText(parentActivity, "Not yet implemented.", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.noAccountTv)
    public void onNoAccountTvClicked() {
        Helper_Fragments.replaceFragment(parentActivity, RegisterFragment.newInstance(), true, RegisterFragment.TAG);
    }
    //</editor-fold>

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        parentActivity = (LoginActivity) getActivity();
        ButterKnife.bind(this, v);

        mAuth = FirebaseAuth.getInstance();

        return v;
    }

    private void checkIfErrorNeeded(TextInputLayout inputLayout, TextInputEditText inputEt) {
        if (TextUtils.isEmpty(inputEt.getText())) {
            inputLayout.setError("This is required");
            mIsAllFilled = false;
        } else {
            inputLayout.setErrorEnabled(false);
        }
    }
}
