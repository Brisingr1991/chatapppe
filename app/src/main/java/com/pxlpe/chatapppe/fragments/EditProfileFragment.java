package com.pxlpe.chatapppe.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;
import com.pxlpe.chatapppe.models.User;
import com.pxlpe.chatapppe.steganography.Steg;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.pxlpe.chatapppe.Constants.ENCODED_PICTURE;
import static com.pxlpe.chatapppe.Constants.FIRST_NAME;
import static com.pxlpe.chatapppe.Constants.LAST_NAME;
import static com.pxlpe.chatapppe.Constants.USERS;


//TODO: check this site for reference: https://github.com/stealthcopter/steganography

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {
    //<editor-fold desc="Variables">
    public static final String TAG = "EditProfileFragment";
    private MainActivity parentActivity;
    private File currentPicture;
    private boolean mIsAllFilled = true;
    private DatabaseReference profileRef;
    private User user;
    private Bitmap currentBitmap;
    private MaterialDialog dialogProgress;
    //</editor-fold>
    //<editor-fold desc="BindViews">
    @BindView(R.id.hiddenTextInputEt)
    TextInputEditText hiddenTextInputEt;
    @BindView(R.id.firstNameInputEt)
    TextInputEditText firstNameInputEt;
    @BindView(R.id.lastNameInputEt)
    TextInputEditText lastNameInputEt;
    @BindView(R.id.editProfileIcon)
    ImageView editProfileIcon;
    @BindView(R.id.hiddenTextInputLayout)
    TextInputLayout hiddenTextInputLayout;
    @BindView(R.id.firstNameInputLayout)
    TextInputLayout firstNameInputLayout;
    @BindView(R.id.lastNameInputLayout)
    TextInputLayout lastNameInputLayout;
    @BindView(R.id.updateBtn)
    Button updateBtn;
    @BindView(R.id.testLayout)
    ScrollView testLayout;

    //</editor-fold>
    //<editor-fold desc="OnClicks">
    @OnClick(R.id.editProfileIcon)
    public void onProfileIconClicked() {
        EasyImage.openChooserWithGallery(this, "Choose your user picture", 0);
    }

    @OnClick(R.id.updateBtn)
    public void onUpdateBtnClicked() {
        updateBtn.setEnabled(false);

        checkIfErrorNeeded(hiddenTextInputLayout, hiddenTextInputEt);
        checkIfErrorNeeded(firstNameInputLayout, firstNameInputEt);
        checkIfErrorNeeded(lastNameInputLayout, lastNameInputEt);

        if (!mIsAllFilled) {
            mIsAllFilled = true;
            updateBtn.setEnabled(true);

            Snackbar.make(testLayout, "Please fill in all fields", Snackbar.LENGTH_SHORT).show();
            return;
        }

        dialogProgress = new MaterialDialog.Builder(parentActivity)
                .autoDismiss(false).cancelable(false)
                .title("Saving...")
                .content("Patience please.")
                .progress(true, 0)
                .build();

        try {
            String hiddenMessage = hiddenTextInputEt.getText().toString();

            Bitmap bitmap;
            if (currentPicture != null) {
                bitmap = Steg.withInput(currentPicture).encode(hiddenMessage).intoBitmap();
            } else {
                byte[] decodedByte = Base64.decode(user.getEncoded_picture(), 0);
                Bitmap bit = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
                bitmap = Steg.withInput(bit).encode(hiddenMessage).intoBitmap();
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String imageEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            Map<String, Object> updateUserMap = new HashMap<>();
            updateUserMap.put(FIRST_NAME, firstNameInputEt.getText().toString());
            updateUserMap.put(LAST_NAME, lastNameInputEt.getText().toString());
            updateUserMap.put(ENCODED_PICTURE, imageEncoded);

            profileRef.updateChildren(updateUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    dialogProgress.dismiss();
                    Helper_Fragments.popBackstack(parentActivity);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            updateBtn.setEnabled(true);
            dialogProgress.dismiss();
        }

    }
    //</editor-fold>

    private void checkIfErrorNeeded(TextInputLayout textInputLayout, TextInputEditText textInputEt) {
        if (TextUtils.isEmpty(textInputEt.getText())) {
            textInputLayout.setError("This is required");
            mIsAllFilled = false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
    }

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, v);
        parentActivity = (MainActivity) getActivity();
        if (parentActivity != null) {
            parentActivity.setBottomNavBar(false);
        }
        FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mCurrentUser != null) {
            profileRef = FirebaseDatabase.getInstance().getReference(USERS).child(mCurrentUser.getUid());

            profileRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        firstNameInputEt.setText(user.getFirst_name());
                        lastNameInputEt.setText(user.getLast_name());

                        if (user.getEncoded_picture() != null) {
                            try {
                                byte[] decodedByte = Base64.decode(user.getEncoded_picture(), 0);
                                currentBitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
                                hiddenTextInputEt.setText(Steg.withInput(currentBitmap).decode().intoString());
                                Glide.with(parentActivity).load(currentBitmap).into(editProfileIcon);
                                editProfileIcon.setImageTintMode(null);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, parentActivity, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                switch (type) {
                    case 0:
                        currentPicture = imageFiles.get(0);

                        try {
                            Glide.with(EditProfileFragment.this).load(currentPicture).into(editProfileIcon);
                            editProfileIcon.setImageTintMode(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        Toast.makeText(parentActivity, "Something went wrong, caution!!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }
}
