package com.pxlpe.chatapppe.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.LoginActivity;
import com.pxlpe.chatapppe.activities.MainActivity;
import com.pxlpe.chatapppe.models.User;
import com.pxlpe.chatapppe.steganography.Steg;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.pxlpe.chatapppe.Constants.USERS;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    //<editor-fold desc="Variables">
    public static final String TAG = "UsersFragment";
    private MainActivity parentActivity;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String userId;
    private User user;
    //</editor-fold>
    //<editor-fold desc="BindView">
    @BindView(R.id.editProfileIcon)
    CircleImageView profileIcon;
    @BindView(R.id.firstNameTv)
    TextView firstNameTv;
    @BindView(R.id.lastNameTv)
    TextView lastNameTv;
    @BindView(R.id.editBtn)
    Button editBtn;
    @BindView(R.id.logoutBtn)
    Button logoutBtn;
    //</editor-fold>
    //<editor-fold desc="OnClick">
    @OnClick(R.id.editProfileIcon)
    public void onProfileIconClicked() {
        try {
            byte[] decodedByte = Base64.decode(user.getEncoded_picture(), 0);
            Bitmap bit = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
            String string = Steg.withInput(bit).decode().intoString();

            Toast.makeText(parentActivity, string, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.editBtn)
    public void onEditBtnClicked() {
        Helper_Fragments.replaceFragment(parentActivity, EditProfileFragment.newInstance(), true, EditProfileFragment.TAG);
    }

    @OnClick(R.id.logoutBtn)
    public void onLogoutBtnClicked() {
        mAuth.signOut();

        sendToStart();
    }
    //</editor-fold>

    public static ProfileFragment newInstance(String key) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString("user_id", key);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!userId.equals(mCurrentUser.getUid())) {
            parentActivity.setBottomNavBar(false);
            if (user != null) {
                parentActivity.setTitleToolbar(user.getFirst_name() + " " + user.getLast_name());
            }
        } else {
            parentActivity.setBottomNavBar(true);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        parentActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, v);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        if (getArguments() != null) {
            userId = getArguments().getString("user_id");
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(USERS).child(userId);

        if (!userId.equals(mCurrentUser.getUid())) {
            editBtn.setVisibility(View.GONE);
            logoutBtn.setVisibility(View.GONE);
            parentActivity.setBottomNavBar(false);
        } else {
            editBtn.setVisibility(View.VISIBLE);
            logoutBtn.setVisibility(View.VISIBLE);
            parentActivity.setBottomNavBar(true);
        }

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    firstNameTv.setText(user.getFirst_name());
                    lastNameTv.setText(user.getLast_name());

                    if (user.getEncoded_picture() != null) {
                        try {
                            byte[] decodedByte = Base64.decode(user.getEncoded_picture(), 0);
                            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
                            Glide.with(ProfileFragment.this).load(bitmap).into(profileIcon);
                            profileIcon.setImageTintMode(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    parentActivity.setTitleToolbar(user.getFirst_name() + " " + user.getLast_name());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }


    private void sendToStart() {
        Intent startIntent = new Intent(parentActivity, LoginActivity.class);
        startActivity(startIntent);
        parentActivity.finish();
        Helper_Fragments.popEntireBackstack(parentActivity);
    }
}
