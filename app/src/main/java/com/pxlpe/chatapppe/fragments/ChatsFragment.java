package com.pxlpe.chatapppe.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseIndexRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.MyApplication;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.activities.MainActivity;
import com.pxlpe.chatapppe.models.Chat;
import com.pxlpe.chatapppe.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.pxlpe.chatapppe.Constants.CHATS;
import static com.pxlpe.chatapppe.Constants.USERS;

public class ChatsFragment extends Fragment {
    private DatabaseReference mChatsReference, mFriendReference;
    public static final String TAG = "UsersFragment";
    private MainActivity parentActivity;

    @BindView(R.id.chatsList)
    RecyclerView chatsList;
    @BindView(R.id.emptyView)
    TextView emptyView;

    @Override
    public void onResume() {
        super.onResume();
        parentActivity.setBottomNavBar(true);
        parentActivity.setTitleToolbar("Chats");
    }

    public static ChatsFragment newInstance() {
        ChatsFragment fragment = new ChatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ChatsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        chatsList.getRecycledViewPool().clear();

        chatsList.setAdapter(new FirebaseIndexRecyclerAdapter<Chat, ChatsViewHolder>
                (Chat.class, R.layout.item_list_chats, ChatsViewHolder.class, mChatsReference, mChatsReference) {
            @Override
            protected void populateViewHolder(final ChatsViewHolder viewHolder, Chat model, int position) {
                final String chatId = model.getChat_id();
                final String friendId = model.getFriend_id();

                mFriendReference.child(friendId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);

                        if (user != null) {
                            viewHolder.setFirstName(user.getFirst_name());
                            viewHolder.setLastName(user.getLast_name());
                            if (user.getEncoded_picture() != null) {
                                viewHolder.setUserPicture(user.getEncoded_picture());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Helper_Fragments.replaceFragment(parentActivity,
                                ConversationFragment.newInstance(chatId, friendId),
                                true, ConversationFragment.TAG);
                    }
                });
                updateLayout();
            }
        });

        updateLayout();
    }

    private void updateLayout() {
        if (chatsList.getAdapter().getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chats, container, false);
        parentActivity = (MainActivity) getActivity();
        ButterKnife.bind(this, v);

        parentActivity.setBottomNavBar(true);
        parentActivity.setTitleToolbar("Chats");

        FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (mCurrentUser != null) {
            mChatsReference = FirebaseDatabase.getInstance().getReference()
                    .child(USERS).child(mCurrentUser.getUid()).child(CHATS);

            mFriendReference = FirebaseDatabase.getInstance().getReference().child(USERS);

            chatsList.setHasFixedSize(true);
            chatsList.setLayoutManager(new LinearLayoutManager(parentActivity));
        }

        return v;
    }

    public static class ChatsViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ChatsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setFirstName(String firstName) {
            TextView firstNameTv = mView.findViewById(R.id.firstNameTv);
            firstNameTv.setText(firstName);
        }

        void setLastName(String lastName) {
            TextView lastNameTv = mView.findViewById(R.id.lastNameTv);
            lastNameTv.setText(lastName);
        }

        void setUserPicture(String pictureEncoded) {
            CircleImageView circleImageView = mView.findViewById(R.id.userIcon);
            byte[] decodedByte = Base64.decode(pictureEncoded, 0);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
            Glide.with(MyApplication.getContext()).load(bitmap).into(circleImageView);
        }
    }
}
