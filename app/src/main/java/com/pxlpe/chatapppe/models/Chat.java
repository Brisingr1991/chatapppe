package com.pxlpe.chatapppe.models;

public class Chat {
    private String chat_id;
    private String friend_id;

    public Chat() {
    }

    public String getChat_id() {

        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(String friend_id) {
        this.friend_id = friend_id;
    }
}
