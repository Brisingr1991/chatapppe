package com.pxlpe.chatapppe.models;

public class User {
    private String first_name;
    private String last_name;
    private String public_exponent;
    private String public_modulus;
    private String encoded_picture;

    public User() {
    }

    public String getEncoded_picture() {
        return encoded_picture;
    }

    public void setEncoded_picture(String encoded_picture) {
        this.encoded_picture = encoded_picture;
    }

    public String getPublic_exponent() {
        return public_exponent;
    }

    public void setPublic_exponent(String public_exponent) {
        this.public_exponent = public_exponent;
    }

    public String getPublic_modulus() {
        return public_modulus;
    }

    public void setPublic_modulus(String public_modulus) {
        this.public_modulus = public_modulus;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
