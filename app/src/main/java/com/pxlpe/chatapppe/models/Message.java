package com.pxlpe.chatapppe.models;

public class Message {
    private String body;
    private String sender;
    private Long timestamp;
    private String encrypted_message;
    private String encrypted_sym_key_friend;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getEncrypted_message() {
        return encrypted_message;
    }

    public void setEncrypted_message(String encrypted_message) {
        this.encrypted_message = encrypted_message;
    }

    public String getEncrypted_sym_key_friend() {
        return encrypted_sym_key_friend;
    }

    public void setEncrypted_sym_key_friend(String encrypted_sym_key_friend) {
        this.encrypted_sym_key_friend = encrypted_sym_key_friend;
    }

    public String getSigned_hash_from_message() {
        return signed_hash_from_message;
    }

    public void setSigned_hash_from_message(String signed_hash_from_message) {
        this.signed_hash_from_message = signed_hash_from_message;
    }

    public String getEncrypted_sym_key_sender() {
        return encrypted_sym_key_sender;
    }

    public void setEncrypted_sym_key_sender(String encrypted_sym_key_sender) {
        this.encrypted_sym_key_sender = encrypted_sym_key_sender;
    }

    private String signed_hash_from_message;
    private String encrypted_sym_key_sender;

    public Message() {
    }


}
