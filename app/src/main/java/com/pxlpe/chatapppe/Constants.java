package com.pxlpe.chatapppe;

/**
 * Created by christianobolla on 13/01/2018.
 */

public class Constants {
    public static final String USERS = "users";
    public static final String FRIEND_ID = "friend_id";
    public static final String CHATS = "chats";
    public static final String CHAT_ID = "chat_id";
    public static final String MESSAGES = "messages";

    // User
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PUBLIC_KEY = "public_key";
    public static final String PUBLIC_EXPONENT = "public_exponent";
    public static final String PUBLIC_MODULUS = "public_modulus";

    public static final String ANDROID_KEYSTORE = "AndroidKeyStore";
    public static final String ENCODED_PICTURE = "encoded_picture";
}
