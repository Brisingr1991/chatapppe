package com.pxlpe.chatapppe.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.pxlpe.chatapppe.Helpers.Helper_Fragments;
import com.pxlpe.chatapppe.R;
import com.pxlpe.chatapppe.fragments.ChatsFragment;
import com.pxlpe.chatapppe.fragments.ProfileFragment;
import com.pxlpe.chatapppe.fragments.UsersFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    //<editor-fold desc="BindView">
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.bottomNavBar)
    BottomBar bottomNavBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.subTitleTv)
    TextView subTitleTv;
    @BindView(R.id.backArrowImg)
    ImageView backArrowImg;
    //</editor-fold>

    @OnClick(R.id.backArrowImg)
    public void onBackArrowImgClicked() {
        Helper_Fragments.popBackstack(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Helper_Fragments.replaceFragment(this, UsersFragment.newInstance(), false, UsersFragment.TAG);

        bottomNavBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                switch (tabId) {
                    case R.id.action_users:
                        Helper_Fragments.replaceFragment(MainActivity.this, UsersFragment.newInstance(), false, UsersFragment.TAG);
                        break;
                    case R.id.action_chats:
                        Helper_Fragments.replaceFragment(MainActivity.this, ChatsFragment.newInstance(), false, ChatsFragment.TAG);
                        break;
                    case R.id.action_profile:
                        if (mAuth.getCurrentUser() != null) {
                            Helper_Fragments.replaceFragment(MainActivity.this, ProfileFragment.newInstance(mAuth.getCurrentUser().getUid()), false, ProfileFragment.TAG);
                        }
                        break;
                }
            }
        });

        final FragmentManager fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (fm.getBackStackEntryCount() > 0) {
                    backArrowImg.setVisibility(View.VISIBLE);
                } else {
                    backArrowImg.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setTitleToolbar(String title) {
        titleTv.setText(title);
        subTitleTv.setVisibility(View.GONE);
    }

//    public void setSubTitleToolbar(String subTitle) {
//        subTitleTv.setText(subTitle);
//        subTitleTv.setVisibility(View.VISIBLE);
//    }

    public void setBottomNavBar(boolean isVisible) {
        if (isVisible) {
            bottomNavBar.setVisibility(View.VISIBLE);
        } else {
            bottomNavBar.setVisibility(View.GONE);
        }
    }
}
