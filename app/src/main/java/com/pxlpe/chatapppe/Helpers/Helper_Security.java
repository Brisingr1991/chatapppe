package com.pxlpe.chatapppe.Helpers;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import static com.pxlpe.chatapppe.Constants.ANDROID_KEYSTORE;

public class Helper_Security {

    // if keypair alias doesn't exist yet make a new pair, and return the public key
    // if exists already return the public key
    public static PublicKey createKeyPairsOrGetPublicKey(String Uid) {
        PublicKey publicKey;

        try {
            KeyStore keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);

            if (!keyStore.containsAlias(Uid)) {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
                keyPairGenerator.initialize(
                        new KeyGenParameterSpec.Builder(
                                Uid,
                                KeyProperties.PURPOSE_DECRYPT
                                        | KeyProperties.PURPOSE_ENCRYPT
                                        | KeyProperties.PURPOSE_VERIFY
                                        | KeyProperties.PURPOSE_SIGN)
                                .setDigests(KeyProperties.DIGEST_SHA256)
                                .setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                                .build());

                KeyPair keyPair = keyPairGenerator.generateKeyPair();

                publicKey = keyPair.getPublic();
            } else {
                KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)
                        keyStore.getEntry(Uid, null);
                publicKey = privateKeyEntry.getCertificate().getPublicKey();
            }

        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | InvalidAlgorithmParameterException | NoSuchProviderException | UnrecoverableEntryException e) {
            e.printStackTrace();
            publicKey = null;
        }

        return publicKey;
    }

    // convert the public key from the friend from a string to a public key
    public static RSAPublicKey convertStringToPublicKey(String publicExponent, String publicModulus) {
        RSAPublicKey publicKey;

        try {
            BigInteger modulus = new BigInteger(publicModulus);
            BigInteger exponent = new BigInteger(publicExponent);
            RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, exponent);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = (RSAPublicKey) keyFactory.generatePublic(rsaPublicKeySpec);
        } catch (Exception e) {
            e.printStackTrace();
            publicKey = null;
        }
        return publicKey;
    }

    // encrypt a text with a public key
    public static String encryptWithPublicKey(PublicKey publicKey, String textToEncrypt) {
        try {
            // changed this:
            // Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidKeyStoreBCWorkaround");
            // to this and all seem to work now
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(
                    outputStream, inCipher);
            cipherOutputStream.write(Base64.decode(textToEncrypt, Base64.DEFAULT));
            cipherOutputStream.close();

            byte[] vals = outputStream.toByteArray();
            return Base64.encodeToString(vals, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // decrypt a text with a private key
    public static String decryptWithPrivateKey(PrivateKey privateKey, String textToDecrypt) {
        try {
            Cipher output = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidKeyStoreBCWorkaround");
            output.init(Cipher.DECRYPT_MODE, privateKey);

            CipherInputStream cipherInputStream = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(textToDecrypt, Base64.DEFAULT)), output);
            ArrayList<Byte> values = new ArrayList<>();
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                values.add((byte) nextByte);
            }

            byte[] bytes = new byte[values.size()];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = values.get(i).byteValue();
            }

            return Base64.encodeToString(bytes, Base64.DEFAULT);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // sign a text with a private key
    public static String signWithPrivateKey(PrivateKey privateKey, String textToSign) {
        try {
            byte[] data = Base64.decode(textToSign, Base64.DEFAULT);

            //signature
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initSign(privateKey);
            sig.update(data);
            byte[] signed = sig.sign();

            return Base64.encodeToString(signed, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // verify a text with a public key
    public static boolean verifyWithPublicKey(PublicKey publicKey, String hashFromDecryptedMessage, String signedHashFromMessage) {
        try {
            byte[] originalData = Base64.decode(signedHashFromMessage, Base64.DEFAULT);
            byte[] signatureBytes = Base64.decode(hashFromDecryptedMessage, Base64.DEFAULT);

            //signature
            Signature sig = Signature.getInstance("SHA256withRSA");
            sig.initVerify(publicKey);
            sig.update(signatureBytes);

            return sig.verify(originalData);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}


