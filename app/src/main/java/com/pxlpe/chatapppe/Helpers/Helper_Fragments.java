package com.pxlpe.chatapppe.Helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.pxlpe.chatapppe.R;

public class Helper_Fragments {
//    private static final String TAG = "Helper_Fragments";

    public static void replaceFragment(AppCompatActivity activity, Fragment fragment, boolean addToBackstack, String TAG) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, fragment.getTag());
        if (addToBackstack) {
            fragmentTransaction.addToBackStack(fragment.getTag());
        }
        try {
            fragmentTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void popBackstack(AppCompatActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        fm.popBackStack();
    }

    public static void popEntireBackstack(AppCompatActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        int backstackEntryCount = fm.getBackStackEntryCount();
        for (int i = 0; i < backstackEntryCount; ++i) {
            fm.popBackStack();
        }
    }
}
